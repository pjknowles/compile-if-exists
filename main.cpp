#include <iostream>

class fancy {
 public:
  fancy fancyFunction() { std::cout << "fancyFunction" << std::endl; }
};

class simple {};

#define HAS_MEM_FUNC(func, name)                                        \
    template<typename T, typename Sign>                                 \
    struct name {                                                       \
        typedef char yes[1];                                            \
        typedef char no [2];                                            \
        template <typename U, U> struct type_check;                     \
        template <typename _1> static yes &chk(type_check<Sign, &_1::func > *); \
        template <typename   > static no  &chk(...);                    \
        static bool const value = sizeof(chk<T>(0)) == sizeof(yes);     \
    }

    HAS_MEM_FUNC(fancyFunction,has_fancy);


template<class T>
class driver {
  T obj;
 public:
    typename std::enable_if<has_fancy<T,T>::value,int>::type
        thing() {
    obj.fancyFunction();
  }
};
int main() {
  driver<fancy> f; f.thing();
//  driver<simple> s;
  return 0;
}